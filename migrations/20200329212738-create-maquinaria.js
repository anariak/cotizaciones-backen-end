'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('maquinaria', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      capcidad: {
        type: Sequelize.FLOAT
      },
      cod_proceso: {
        type: Sequelize.STRING
      },
      max: {
        type: Sequelize.FLOAT
      },
      min: {
        type: Sequelize.FLOAT
      },
      desc: {
        type: Sequelize.INTEGER
      },
      consumo_kw: {
        type: Sequelize.FLOAT
      },
      consumo_mo: {
        type: Sequelize.FLOAT
      },
      masa_hora: {
        type: Sequelize.FLOAT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('maquinaria');
  }
};