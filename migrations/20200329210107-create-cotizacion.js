'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('cotizacions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      num_cotizacion: {
        type: Sequelize.STRING
      },
      valor: {
        type: Sequelize.INTEGER
      },
      condicion_pago: {
        type: Sequelize.STRING
      },
      validez: {
        type: Sequelize.STRING
      },
      plazo_entrega: {
        type: Sequelize.DATE
      },
      lugar_entrega: {
        type: Sequelize.STRING
      },
      nota: {
        type: Sequelize.STRING
      },
      estado: {
        type: Sequelize.INTEGER
      },
      rut: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      moneda: {
        type: Sequelize.STRING
      },
      cod_proyecto: {
        type: Sequelize.STRING
      },
      fecha_ingreso: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('cotizacions');
  }
};