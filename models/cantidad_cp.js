'use strict';
module.exports = (sequelize, DataTypes) => {
  const Cantidad_cp = sequelize.define('Cantidad_cp', {
    cod_comp: DataTypes.STRING,
    cod_mp: DataTypes.STRING,
    cantidad: DataTypes.FLOAT
  }, {});
  Cantidad_cp.associate = function(models) {
    // associations can be defined here
  };
  return Cantidad_cp;
};