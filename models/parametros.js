'use strict';
module.exports = (sequelize, DataTypes) => {
  const parametros = sequelize.define('parametros', {
    codigo: DataTypes.STRING,
    valor: DataTypes.FLOAT,
    fecha: DataTypes.DATE
  }, {});
  parametros.associate = function(models) {
    // associations can be defined here
  };
  return parametros;
};