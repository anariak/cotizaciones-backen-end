'use strict';
module.exports = (sequelize, DataTypes) => {
  const Cantidad_mp = sequelize.define('Cantidad_mp', {
    cod_mp: DataTypes.STRING,
    cod_prod: DataTypes.STRING,
    cantidad: DataTypes.FLOAT,
    unidad: DataTypes.STRING
  }, {});
  Cantidad_mp.associate = function(models) {
    // associations can be defined here
  };
  return Cantidad_mp;
};