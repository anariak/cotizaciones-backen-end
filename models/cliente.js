'use strict';
module.exports = (sequelize, DataTypes) => {
  const cliente = sequelize.define('cliente', {
    rut: DataTypes.STRING,
    nombre: DataTypes.STRING,
    imagen: DataTypes.STRING
  }, {});
  cliente.associate = function(models) {
    // associations can be defined here
  };
  return cliente;
};