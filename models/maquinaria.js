'use strict';
module.exports = (sequelize, DataTypes) => {
  const maquinaria = sequelize.define('maquinaria', {
    capcidad: DataTypes.FLOAT,
    cod_proceso: DataTypes.STRING,
    max: DataTypes.FLOAT,
    min: DataTypes.FLOAT,
    desc: DataTypes.INTEGER,
    consumo_kw: DataTypes.FLOAT,
    consumo_mo: DataTypes.FLOAT,
    masa_hora: DataTypes.FLOAT
  }, {});
  maquinaria.associate = function(models) {
    // associations can be defined here
  };
  return maquinaria;
};