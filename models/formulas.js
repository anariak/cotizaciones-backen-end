'use strict';
module.exports = (sequelize, DataTypes) => {
  const formulas = sequelize.define('formulas', {
    codigo: DataTypes.STRING,
    material: DataTypes.STRING,
    nombre: DataTypes.STRING
  }, {});
  formulas.associate = function(models) {
    // associations can be defined here
  };
  return formulas;
};