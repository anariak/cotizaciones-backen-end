'use strict';
module.exports = (sequelize, DataTypes) => {
  const planos = sequelize.define('planos', {
    codigo: DataTypes.STRING,
    valor: DataTypes.FLOAT,
    archivo: DataTypes.STRING,
    amortizacion: DataTypes.INTEGER,
    descripcion: DataTypes.STRING
  }, {});
  planos.associate = function(models) {
    // associations can be defined here
  };
  return planos;
};