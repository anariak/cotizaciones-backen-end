'use strict';
module.exports = (sequelize, DataTypes) => {
  const compuesto = sequelize.define('compuesto', {
    codigo: DataTypes.STRING,
    tipo: DataTypes.STRING,
    valor: DataTypes.INTEGER,
    nombre: DataTypes.STRING,
    densidad: DataTypes.FLOAT
  }, {});
  compuesto.associate = function(models) {
    // associations can be defined here
  };
  return compuesto;
};