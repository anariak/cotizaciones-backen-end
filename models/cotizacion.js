'use strict';
module.exports = (sequelize, DataTypes) => {
  const cotizacion = sequelize.define('cotizacion', {
    num_cotizacion: DataTypes.STRING,
    valor: DataTypes.INTEGER,
    condicion_pago: DataTypes.STRING,
    validez: DataTypes.STRING,
    plazo_entrega: DataTypes.DATE,
    lugar_entrega: DataTypes.STRING,
    nota: DataTypes.STRING,
    estado: DataTypes.INTEGER,
    rut: DataTypes.STRING,
    email: DataTypes.STRING,
    moneda: DataTypes.STRING,
    cod_proyecto: DataTypes.STRING,
    fecha_ingreso: DataTypes.DATE
  }, {});
  cotizacion.associate = function(models) {
    // associations can be defined here
  };
  return cotizacion;
};