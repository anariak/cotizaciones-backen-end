'use strict';
module.exports = (sequelize, DataTypes) => {
  const moldes = sequelize.define('moldes', {
    codigo: DataTypes.STRING,
    valor: DataTypes.FLOAT,
    plano: DataTypes.STRING,
    amortizacion: DataTypes.INTEGER,
    descripcion: DataTypes.STRING,
    molde_padre: DataTypes.STRING
  }, {});
  moldes.associate = function(models) {
    // associations can be defined here
  };
  return moldes;
};