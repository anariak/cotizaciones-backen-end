'use strict';
module.exports = (sequelize, DataTypes) => {
  const producto_materiales = sequelize.define('producto_materiales', {
    codigo: DataTypes.STRING,
    codigo_producto: DataTypes.STRING,
    nombre: DataTypes.STRING,
    cantidad: DataTypes.FLOAT,
    tipo: DataTypes.STRING,
    unidad: DataTypes.STRING,
    valor: DataTypes.FLOAT
  }, {});
  producto_materiales.associate = function(models) {
    // associations can be defined here
  };
  return producto_materiales;
};