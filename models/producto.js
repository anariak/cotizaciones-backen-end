'use strict';
module.exports = (sequelize, DataTypes) => {
  const producto = sequelize.define('producto', {
    codigo: DataTypes.STRING,
    nombre: DataTypes.STRING,
    formula: DataTypes.STRING,
    fecha_ingreso: DataTypes.DATE,
    cod_plano: DataTypes.STRING,
    unidad: DataTypes.STRING
  }, {});
  producto.associate = function(models) {
    // associations can be defined here
  };
  return producto;
};