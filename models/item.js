'use strict';
module.exports = (sequelize, DataTypes) => {
  const item = sequelize.define('item', {
    codigo_producto: DataTypes.STRING,
    num_cotizacion: DataTypes.STRING,
    descripcion: DataTypes.STRING,
    cantidades: DataTypes.INTEGER,
    precio_unidad: DataTypes.FLOAT,
    ajuste_variacion: DataTypes.FLOAT
  }, {});
  item.associate = function(models) {
    // associations can be defined here
  };
  return item;
};