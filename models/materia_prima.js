'use strict';
module.exports = (sequelize, DataTypes) => {
  const materia_prima = sequelize.define('materia_prima', {
    codigo: DataTypes.STRING,
    valor: DataTypes.FLOAT,
    unidad: DataTypes.STRING,
    tipo: DataTypes.STRING,
    nombre: DataTypes.STRING
  }, {});
  materia_prima.associate = function(models) {
    // associations can be defined here
  };
  return materia_prima;
};