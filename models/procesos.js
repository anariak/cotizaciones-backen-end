'use strict';
module.exports = (sequelize, DataTypes) => {
  const procesos = sequelize.define('procesos', {
    codigo: DataTypes.STRING
  }, {});
  procesos.associate = function(models) {
    // associations can be defined here
  };
  return procesos;
};