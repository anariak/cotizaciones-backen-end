'use strict';
module.exports = (sequelize, DataTypes) => {
  const feedback = sequelize.define('feedback', {
    n_cotizacion: DataTypes.STRING,
    obs: DataTypes.STRING,
    fecha: DataTypes.DATE,
    email: DataTypes.STRING
  }, {});
  feedback.associate = function(models) {
    // associations can be defined here
  };
  return feedback;
};