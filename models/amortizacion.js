'use strict';
module.exports = (sequelize, DataTypes) => {
  const Amortizacion = sequelize.define('Amortizacion', {
    molde: DataTypes.STRING,
    procentage: DataTypes.INTEGER,
    item: DataTypes.INTEGER
  }, {});
  Amortizacion.associate = function(models) {
    // associations can be defined here
  };
  return Amortizacion;
};