'use strict';
module.exports = (sequelize, DataTypes) => {
  const lista_procesos = sequelize.define('lista_procesos', {
    cod_formula: DataTypes.STRING,
    codigo_proceso: DataTypes.STRING,
    perdida: DataTypes.INTEGER
  }, {});
  lista_procesos.associate = function(models) {
    // associations can be defined here
  };
  return lista_procesos;
};